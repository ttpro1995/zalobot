package me.thaithien.zalobot;
import com.google.gson.JsonObject;
import com.vng.zalo.sdk.oa.ZaloOaClient;
import com.vng.zalo.sdk.oa.ZaloOaInfo;
import me.thaithien.zalobot.Handler.DucklingHandler;
import me.thaithien.zalobot.Handler.HandlerBroker;
import me.thaithien.zalobot.Handler.VitkTokHandler;
import me.thaithien.zalobot.Model.VnCoreNlpWorker;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;

import static spark.Spark.*;
public class MainApp {
    public static void main(String[] args) {

        VitkTokHandler.init();

        long oaid = Long.valueOf(System.getenv("OAID")); // put your oaid here
        String secrect = System.getenv("OA_SECRET"); // put secret her

        ZaloOaInfo info = new ZaloOaInfo(oaid, secrect);
        ZaloOaClient oaClient = new ZaloOaClient(info);
        // VnCoreNlpWorker worker = VnCoreNlpWorker.getInstance();


        get("/zalowebhook", (req, res) -> {
            String zaloQStr = req.queryString();
            res.status(200);
            System.out.println(zaloQStr);
            String fromId = req.queryParams("fromuid");
            String msgStr = req.queryParams("message");
            ZaloReceiveMessage msg = new ZaloReceiveMessage(fromId, msgStr, req.queryParams("timestamp"));

            // String sendStr = HandlerBroker.process(msg);
            String sendStr = DucklingHandler.processPlain(msg);
            JsonObject ret = oaClient.sendTextMessage(Long.valueOf(fromId), sendStr);

            System.out.println(msg);
            System.out.println(fromId);
            System.out.println(ret);
            return "meow";
        });


    }
}

