package me.thaithien.zalobot.Handler;
import ai.vitk.pos.Tagger;
import ai.vitk.type.Token;
import com.google.common.collect.Lists;
import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;

import java.util.List;
import java.util.StringJoiner;

/**
 * http://mim.hus.vnu.edu.vn/dsl/tools/tagger
 */
public class VitkTagHandler {
    public static Tagger vitkTagger = null;

    public static void init(){
        vitkTagger = new Tagger();
    }

    public static String process(ZaloReceiveMessage msg){
        if (vitkTagger == null){
            VitkTagHandler.init();
        }

        String inStr = msg.message.substring(CONSTANT.POSTAG_VITK.length()); // remove command tag
        List<Token> toks = Lists.newArrayList(vitkTagger.tag(inStr).iterator());
        StringJoiner joiner = new StringJoiner(" ");
        for (Token tok : toks){
            joiner.add(tok.toString());
        }
        String result = joiner.toString();
        return result;
    }
}
