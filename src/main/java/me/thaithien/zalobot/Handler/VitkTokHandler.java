package me.thaithien.zalobot.Handler;

import ai.vitk.tok.Tokenizer;
import ai.vitk.type.Token;
import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;

import java.util.List;
import java.util.StringJoiner;


public class VitkTokHandler {

    private static Tokenizer vitkTokenizer;
    public static void init(){
        vitkTokenizer = new Tokenizer();
    }

    public static String process(ZaloReceiveMessage msg){

        if (vitkTokenizer == null){
            VitkTokHandler.init();
        }
        String inStr = msg.message.substring(CONSTANT.TOK_VITK.length()); // remove command tag
        String result = vitkTokenizer.tokenize(inStr).toString();


        return result;
    }

    public static String tokenize(String inStr){
        if (vitkTokenizer == null){
            VitkTokHandler.init();
        }

        List<Token> tokens = vitkTokenizer.tokenize(inStr);
        StringJoiner joiner = new StringJoiner(" ");
        for (Token tok : tokens){
            joiner.add(tok.getWord().replace(" ", "_"));
        }
        return joiner.toString();
    }
}
