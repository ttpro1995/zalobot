package me.thaithien.zalobot.Handler;

import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.VnCoreNlpWorker;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;
import vn.corenlp.postagger.PosTagger;
import vn.pipeline.VnCoreNLP;
import vn.pipeline.Word;

import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;

public class VnCoreNlpHandler {
    public static PosTagger tagger = null;

    public static String process(ZaloReceiveMessage msg){
        String inStr = msg.message.substring(CONSTANT.POSTAG_VITKPOS_VNCORENLPTAG.length()); // remove command tag
        return processStr(inStr);
    }

    public static String processStr(String inStr) {

        VnCoreNlpWorker worker = VnCoreNlpWorker.getInstance();

        List<Word> result = null;
        try {
            result = worker.process(inStr);
        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        }
        StringJoiner joiner = new StringJoiner("\n");
        for (Word w : result) {
            joiner.add(w.toString());
        }
        return joiner.toString();
    }



}
