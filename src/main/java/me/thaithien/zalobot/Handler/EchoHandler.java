package me.thaithien.zalobot.Handler;

import com.google.gson.JsonObject;
import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;

public class EchoHandler {
    public static String process(ZaloReceiveMessage msg){

        return "echo " + msg.message;
    }
}
