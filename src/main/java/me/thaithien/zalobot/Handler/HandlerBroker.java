package me.thaithien.zalobot.Handler;

import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.VnCoreNlpWorker;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;

public class HandlerBroker {

    public static String process(ZaloReceiveMessage msg){
        long startTime = System.currentTimeMillis();
        String msgStr = msg.message;
        String result = null;

        if (msgStr.startsWith(CONSTANT.TOK_VITK)){
            result = VitkTokHandler.process(msg);
        } else if (msgStr.startsWith(CONSTANT.POSTAG_VITK)) {
            result = VitkTagHandler.process(msg);
        } // else if (msgStr.startsWith(CONSTANT.POSTAG_VITKPOS_VNCORENLPTAG)){
//            result = VnCoreNlpHandler.process(msg);
//        }
        else {
            result = EchoHandler.process(msg);
        }

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println(elapsedTime);

        return elapsedTime + "      \n" + result;
    }
}
