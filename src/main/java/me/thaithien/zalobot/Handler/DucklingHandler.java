package me.thaithien.zalobot.Handler;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;

import java.net.URI;

public class DucklingHandler {
    //curl -XPOST http://0.0.0.0:8000/parse --data 'locale=en_GB&text=tomorrow at eight'
    static String ducklingUrl = "http://206.189.84.191:9829/parse";


    public static String process(ZaloReceiveMessage msg){

        String inStr = msg.message.substring(CONSTANT.DUCKLING.length()); // remove command tag
        return processStr(inStr);
    }

    /**
     * msg without command tag
     * @param msg
     * @return
     */
    public static String processPlain(ZaloReceiveMessage msg){
        return processStr(msg.message);
    }

    public static String processStr(String str){
        String data = "locale=vi_VN&text=zTextz";
        data = data.replace("zTextz", str);
        HttpRequest request =  HttpRequest.post(ducklingUrl).send(data);
        String response = request.body();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(response);
        String prettyJsonString = gson.toJson(je);
        return prettyJsonString;
    }


}
