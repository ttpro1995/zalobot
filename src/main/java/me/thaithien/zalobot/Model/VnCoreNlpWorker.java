/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.thaithien.zalobot.Model;

import ai.vitk.tok.Tokenizer;
import ai.vitk.type.Token;
import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.corenlp.ner.NerRecognizer;
import vn.corenlp.parser.DependencyParser;
import vn.corenlp.postagger.PosTagger;
import vn.pipeline.Word;

/**
 *
 * @author thient <thient@vng.com.vn>
 */
public class VnCoreNlpWorker {
    public static final Logger LOGGER = LoggerFactory.getLogger(VnCoreNlpWorker.class);
    Tokenizer vitkTokenizer;
    PosTagger tagger;
    NerRecognizer ner;
    DependencyParser parser;

    public VnCoreNlpWorker() throws IOException {
        vitkTokenizer = new Tokenizer();
        tagger = new PosTagger();
        ner = new NerRecognizer();
        parser = new DependencyParser();
    }

    public static VnCoreNlpWorker INSTANCE = null;

    public static VnCoreNlpWorker getInstance(){
        if (INSTANCE == null){
            try {
                INSTANCE = new VnCoreNlpWorker();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return INSTANCE;
    }

    public List<Word> process(String inStr) throws IOException {
        long startTime = System.currentTimeMillis();
        List<Token> tokens = vitkTokenizer.tokenize(inStr);
        StringJoiner joiner = new StringJoiner(" ");
        for (Token tok : tokens) {
            joiner.add(tok.getWord().replace(" ", "_"));
        }
        String tokSentence = joiner.toString();
        
        List<Word> tagSentence = tagger.tagSentence(tokSentence);
        ner.tagSentence(tagSentence);
        parser.tagSentence(tagSentence);
        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - startTime;
        LOGGER.info("time " + elapsedTime);
        return tagSentence;
    }
}
