package me.thaithien.zalobot.Model;

/**
 * An Message get from webhook
 */
public class ZaloReceiveMessage {
    public final String fromuid;
    public final String message;
    public final String timestamp;

    public ZaloReceiveMessage(String fromuid, String message, String timestamp) {
        this.fromuid = fromuid;
        this.message = message;
        this.timestamp = timestamp;
    }
}
