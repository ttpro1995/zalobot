package me.thaithien.zalobot.Handler;

import org.junit.Test;

import static org.junit.Assert.*;

public class VitkTokHandlerTest {

    @Test
    public void tokenize() {
        VitkTokHandler vitkTokHandler = new VitkTokHandler();
        String result = vitkTokHandler.tokenize("UBND xã Hương Hòa (huyện Nam Đông, Thừa Thiên - Huế) vừa ban hành quyết định xử phạt vi phạm hành chính đối với bà Nguyễn Thị Lỗi (44 tuổi) vì chuyển mục đích sử dụng đất nông nghiệp trái phép.");
        System.out.println(result);
    }
}