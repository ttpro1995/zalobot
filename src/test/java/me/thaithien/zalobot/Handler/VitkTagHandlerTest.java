package me.thaithien.zalobot.Handler;

import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;
import org.junit.Test;

import static org.junit.Assert.*;

public class VitkTagHandlerTest {

    @Test
    public void process() {
        VitkTagHandler.init();
        long startTime = System.currentTimeMillis();
        String sentence = CONSTANT.POSTAG_VITK + "Bộ Công an đã cử đại diện Cục An ninh chính trị nội bộ (A83) chủ trì đoàn thanh tra, kiểm tra, giám sát vụ gian lận điểm thi tại Hà Giang";
//        "Bộ Công an đã cử đại diện Cục An ninh chính trị nội bộ (A83) chủ trì đoàn thanh tra, kiểm tra, giám sát vụ gian lận điểm thi tại Hà Giang";
        ZaloReceiveMessage msg = new ZaloReceiveMessage("1", sentence, "1");
        String result = VitkTagHandler.process(msg);
        System.out.println(result);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println(elapsedTime);
        System.out.println(elapsedTime);
    }
}