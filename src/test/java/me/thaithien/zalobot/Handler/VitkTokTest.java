package me.thaithien.zalobot.Handler;

import me.thaithien.zalobot.CONSTANT;
import me.thaithien.zalobot.Model.ZaloReceiveMessage;
import org.junit.Test;

public class VitkTokTest {

    @Test
    public void init() {
        VitkTokHandler.init();
    }

    @Test
    public void process() {
        VitkTokHandler.init();

        long startTime = System.currentTimeMillis();
        String inStr = CONSTANT.TOK_VITK + "Đáng chú ý, đại biểu HĐND đã tiến hành biểu quyết miễn nhiệm chức danh Ủy viên UBND tỉnh Quảng Nam (nhiệm kỳ 2016-2021) của ông Lê Phước Hoài Bảo, nguyên Giám đốc sở Kế hoạch và Đầu tư (KH-ĐT) tỉnh này.";
        ZaloReceiveMessage msg = new ZaloReceiveMessage("111", inStr, "111");
        String strResult = VitkTokHandler.process(msg);
        System.out.println(strResult);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println(elapsedTime);
    }
}